const gulp = require("gulp");
const nodemon = require("gulp-nodemon");
const path = require("path");
const browserSync = require("browser-sync").create({});

const baseDir = path.join(__dirname, "..");
const buildFolder = path.join(baseDir, "build");

const ts = require("./typescript");
ts.handleTs(gulp, buildFolder, browserSync);

gulp.task("reload", function () {
    return browserSync.reload({ stream: true });
});

function initBrowser() {
    browserSync.init({
        server: buildFolder,
        port: 3005,
        logLevel: "info",
        logPrefix: "BS",
        online: false,
        xip: false,
        notify: false,
        reloadDebounce: 100,
        reloadOnRestart: true,
        watchEvents: ["add", "change"],
    });
}

gulp.task("dev", gulp.series("scripts"));

gulp.task("watch", function () {
    initBrowser();
    gulp.watch("../src/js/**/*.ts", gulp.series("scripts"));
});

gulp.task("default", gulp.series("dev", "watch"));
